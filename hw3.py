# CAAM 519 Problem 3 of implementing simple mixing method using SNESSHELL type in PETSc
#! /usr/bin/env python
import os

sizes       = []
timesNewton = []
timesMixing = []
for k in range(5):
    Nx=10*2**k
    sizes.append(Nx**2)
    modname_Newton='perf_Newton%d'%k
    options_Newton=['-da_grid_x',str(Nx),'-da_grid_y',str(Nx),'-log_view',':%s.py:ascii_info_detail'%modname_Newton]
    os.system('./bin/ex5 '+' '.join(options_Newton))
    perfmod_Newton = __import__(modname_Newton)
    timesNewton.append(perfmod_Newton.Stages['Main Stage']['SNESSolve'][0]['time'])    

    modname_Mixing ='perf_Mixing%d'%k
    options_Mixing=['-pc_type gamg ksp_type gmres -ksp_monitor','-da_grid_x',str(Nx),'-da_grid_y',str(Nx),'-log_view',':%s.py:ascii_info_detail'%modname_Mixing,' -mms 1 ',
                  ' -snes_monitor',' -mixing519']     #extra option to call simple mixing
    os.system('./bin/ex5 '+' '.join(options_Mixing))
    perfmod_Mixing= __import__(modname_Mixing)
    timesMixing.append(perfmod_Mixing.Stages['Main Stage']['SNESSolve'][0]['time'])
print zip(sizes, timesNewton,timesMixing)

from pylab import legend,plot,loglog,show,title,xlabel,ylabel
plot(timesNewton,sizes,'r-',label='Newton')
plot(timesMixing,sizes,'b-',label='SimpleMixing')
title('Problem 3')
xlabel('total work')
ylabel('precison')
legend(loc=1)
show()

loglog(timesNewton,sizes,'r-',label='Newton')
loglog(timesMixing,sizes,'b-',label='SimpleMixing')
title('Problem 3 loglog')
xlabel('total work')
ylabel('precision')
legend(loc='best')
show()

